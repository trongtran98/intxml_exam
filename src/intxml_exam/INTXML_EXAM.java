package intxml_exam;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nux.xom.xquery.XQueryUtil;

public class INTXML_EXAM {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("1. Lấy toàn bộ sinh viên\n2. Tìm theo id");
        System.out.print("Chọn: ");
        int choose = scanner.nextInt();
        if (choose == 1) {
            ArrayList<Student> students = getAllStudents();
            System.out.println("------------------------------------------------");
            System.out.println("Id\tName\t\t\tAddress");
            for (Student s : students) {

                System.out.println(s.toString());
            }
        } else if (choose == 2) {
            System.out.println("------------------------------------------------");
            System.out.print("Nhập ID: ");
            scanner.nextLine();
            String id = scanner.nextLine();
            ArrayList<Student> students = searchById(id);
            System.out.println("Id\tName\t\t\tAddress");
            for (Student s : students) {
                System.out.println(s.toString());
            }
        }
    }

    private static ArrayList<Student> searchById(String filter) {
        ArrayList<Student> arrayList = new ArrayList();
        try {
            Document document = new Builder().build(new File("src/intxml_exam/students.xml"));
            Nodes nodes = XQueryUtil.xquery(document, "//student[id = " + filter + "]");
            for (int i = 0; i < nodes.size(); i++) {
                Student stu = new Student();
                Node node = nodes.get(i);
                stu.setId(node.getChild(1).getValue());
                stu.setName(node.getChild(3).getValue());
                stu.setAddress(node.getChild(5).getValue());
                arrayList.add(stu);
            }
        } catch (ParsingException ex) {
            Logger.getLogger(INTXML_EXAM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(INTXML_EXAM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrayList;
    }
    
    private static ArrayList<Student> getAllStudents() {
        ArrayList<Student> arrayList = new ArrayList();
        try {
            Document document = new Builder().build(new File("src/intxml_exam/students.xml"));
            Nodes nodes = XQueryUtil.xquery(document, "//student");

            for (int i = 0; i < nodes.size(); i++) {
                Student stu = new Student();
                Node node = nodes.get(i);
                stu.setId(node.getChild(1).getValue());
                stu.setName(node.getChild(3).getValue());
                stu.setAddress(node.getChild(5).getValue());
                arrayList.add(stu);
            }

        } catch (ParsingException ex) {
            Logger.getLogger(INTXML_EXAM.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(INTXML_EXAM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrayList;
    }
}
